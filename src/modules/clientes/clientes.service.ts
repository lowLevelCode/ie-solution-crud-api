import { Injectable } from '@nestjs/common';

import * as lowdb from "lowdb"
import * as FileAsync from "lowdb/adapters/FileAsync"

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ClientesService {

    constructor(){}

    async getAll() {
        const adapter = new FileAsync("db.json");
        const db = await lowdb(adapter);

        return db.get('clientes').value();
    }

    async createCliente(cliente:any) {

        cliente.id = uuidv4();

        const adapter = new FileAsync("db.json");
        const db = await lowdb(adapter);

        db.defaults({ clientes: [] }).write();
        db.get('clientes').push(cliente).write();
        
    }

    async updateCliente(id:any, cliente:any){
        const adapter = new FileAsync("db.json");
        const db = await lowdb(adapter);

        db.get('clientes').remove({id:id}).write();
        db.get('clientes').push(cliente).write();
    }

    async deleteCliente(id:any){
        const adapter = new FileAsync("db.json");
        const db = await lowdb(adapter);
        db.get('clientes').remove({id:id}).write();
    }
}
