import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ClientesService } from './clientes.service';

@Controller('clientes')
export class ClientesController {
    constructor(private readonly _clientesService:ClientesService){}

    @Get()
    getAll() {
        return this._clientesService.getAll();
    }


    @Post()
    createCliente(@Body() cliente:any){
        return this._clientesService.createCliente(cliente);
    }

    @Put(':id')
    updateCliente(@Param('id') id:string, @Body() cliente:any){
        return this._clientesService.createCliente(cliente);
    }

    @Delete(':id')
    deleteCliente(@Param('id') id:string){
        return this._clientesService.deleteCliente(id);
    }

}
